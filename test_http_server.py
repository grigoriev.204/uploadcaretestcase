import argparse
import json
import logging
import os
import sys
import time

from http.server import HTTPServer, BaseHTTPRequestHandler

from constants import ENDPOINTS_KEY, STATUS_KEY, CONTENT_KEY, HEADERS_KEY, INFINITE_CONTENT_KEY, \
                        INFINITE_CONTENT_DELAY, HTTP_DEFAULT_HOST, HTTP_DEFAULT_PORT, messages_server


__all__ = ["load_config", "start_server"]

log = logging.getLogger("Server")
logging.basicConfig(stream=sys.stdout, level=logging.INFO)

def start_server(config, hostname, port):
    config = __load_config(config)
    http_server = HTTPServer((hostname, port), RequestHandlerFactory(config))
    log.info("HTTP server started on: %s:%s" % (hostname, port))
    http_server.serve_forever()


def __load_config(config_file):
    config = {}
    if not os.path.isabs(config_file):
        normpath = os.path.abspath(config_file)
    else:
        normpath = config_file
    with open(normpath, "r") as f:
        config = json.load(f)
    __validate_config(config)
    return config


def __validate_config(config):
    # mock of config validation, checks only all of required keys are presented
    endpoints_required_keys = {HEADERS_KEY, STATUS_KEY, CONTENT_KEY, INFINITE_CONTENT_KEY}
    if ENDPOINTS_KEY not in config.keys():
        raise Exception(messages_server.no_endpoints_key)
    else:
        number_of_endpoints = len(config[ENDPOINTS_KEY])
        if number_of_endpoints == 0:
            raise Exception(messages_server.no_endpoints_defined)
        else:
            for endpoint_name, endpoint_config in config[ENDPOINTS_KEY].items():
                endpoint_keys_set = set(endpoint_config.keys())
                if (endpoints_required_keys & endpoint_keys_set) != endpoints_required_keys:
                    raise Exception(messages_server.no_required_fields_in_endpoint % (endpoint_name))


# use factory to provide site scheme (config) to HTTPHandler.
def RequestHandlerFactory(config):

    class TestHTTPHandler(BaseHTTPRequestHandler):
        def __init__(self, *args, **kwargs):
            self.__config = config
            super(TestHTTPHandler, self).__init__(*args, **kwargs)

        def __handle_resource_except_content(self):
            if self.path in self.__config[ENDPOINTS_KEY].keys():
                resource = self.__config[ENDPOINTS_KEY][self.path]
                status = resource[STATUS_KEY]
                self.send_response(resource[STATUS_KEY])
                for header_name, header_value in resource[HEADERS_KEY].items():
                    self.send_header(header_name, header_value)
                self.end_headers()
            else:
                status = 404
                self.send_response(404)
                self.send_header("content-type", "text/html")
                self.end_headers()
            return status

        def __handle_content(self):
            resource = self.__config[ENDPOINTS_KEY][self.path]
            if resource[INFINITE_CONTENT_KEY]:
                # simulate an heavy response, just write to wfile in loop with sleep
                i = 0
                while i < INFINITE_CONTENT_DELAY:
                    self.wfile.write(b"aaaaaa")
                    time.sleep(1)
                    i += 1
            else:
              if resource[CONTENT_KEY]:
                self.wfile.write(bytes(resource[CONTENT_KEY], encoding="utf-8"))

        def do_HEAD(self):
            self.__handle_resource_except_content()

        def do_GET(self):
            status = self.__handle_resource_except_content()
            if status == 200:
                self.__handle_content()
            else:
                self.send_error(404, explain=messages_server.error_404)

    return TestHTTPHandler

def __command_line_parse():
    parser = argparse.ArgumentParser()
    parser.add_argument("config",
                         help="Site configuration file. Contains hostname, port and site endpoints to serve in HTTP server.")
    parser.add_argument("--hostname",
                        help="Specifies the address to which HTTP server should bind. Default value is \"%s\"." % HTTP_DEFAULT_HOST,
                        action="store",
                        default=HTTP_DEFAULT_HOST)
    parser.add_argument("--port",
                        help="Specifies the port number, on which server serves. Default port number is %s." % HTTP_DEFAULT_PORT,
                        action="store",
                        default=HTTP_DEFAULT_PORT)
    args = parser.parse_args()
    if not os.path.exists(args.config):
        raise Exception(messages_server.wrong_config_path)
    return args


if __name__ == "__main__":
    args = __command_line_parse()
    start_server(args.config, args.hostname, args.port)