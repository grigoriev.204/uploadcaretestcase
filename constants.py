ENDPOINTS_KEY = "endpoints"
STATUS_KEY = "status_code"
CONTENT_KEY = "content"
HEADERS_KEY = "headers"
INFINITE_CONTENT_KEY = "infinite_content"

# timeout to simulate HEAVY or INFINITE content in response - 5 minutes
# to simulate infinite response just set INFINITE_CONTENT_DELAY to numpy.inf or math.inf
INFINITE_CONTENT_DELAY = 5*60


# HTTP Server defaults
HTTP_DEFAULT_HOST = "localhost"
HTTP_DEFAULT_PORT = 8000


__messages_client = {
    "cycled_redirects": "Cycled redirects are detected. The URL %s redirects to the beggining of cycle."
}


__messages_server = {
    "error_404": "Page not found",
    "wrong_config_path": "Wrong path to the site configuration file.",
    "no_endpoints_defined": "There is no site endpoints in config file.",
    "no_endpoint_key": "Site configuration should contain the %s key." % ENDPOINTS_KEY,
    "no_required_fields_in_endpoint": "Endpoint '%s' does not contain one of required fields."
}


class __Bunch(dict):
    __getattr__ = dict.__getitem__

messages_client = __Bunch(__messages_client)
messages_server = __Bunch(__messages_server)