import json
import os
import requests
import subprocess
import tempfile
import time
import unittest

from textwrap import dedent

from constants import ENDPOINTS_KEY

HTTP_SERVER_HOSTNAME = "127.0.0.1"
HTTP_SERVER_PORT = 8000

HTTP_SERVER_UP_TIMEOUT = 60

class BaseHttpTest(unittest.TestCase):

    def setUp(self):
        self.__server_process = None
        self._files_to_del = []

    def _start_server(self, path_to_config, bind_argument="", port_argument=""):
        command_to_run_server = ["python3", "test_http_server.py", path_to_config]
        self.__server_process = subprocess.Popen(command_to_run_server)
        print("Server process has been started with pid=%s" % self.__server_process.pid)

    def _wait_server_up(self, hostname, port):
        start_time = time.time()
        while True:
            if time.time() - start_time > HTTP_SERVER_UP_TIMEOUT:
                print("Failed to connect to Test HTTP server.")
                break
            try:
                res = requests.get("http://%s:%s/" % (hostname, port))
                if res.status_code:
                    break
            except Exception:
                time.sleep(1)

    def tearDown(self):
        if self.__server_process:
            self.__server_process.kill()
            self.__server_process.wait()
            print("Server process has been killed.")
        for path in self._files_to_del:
            try:
                os.unlink(path)
            except OSError as e:
                print("Failed to remove file %s. %s" % (path, str(e)))


class TestHttpServer(BaseHttpTest):

    def test_base(self):
        test_config = tempfile.NamedTemporaryFile(delete=False)
        test_config.close()
        config = dedent("""
                    {
                        "endpoints": {
                            "/test" : {
                                "status_code": 200,
                                "content": "Test content",
                                "headers": {},
                                "infinite_content": false
                            },
                            "/redirect" : {
                                "status_code": 301,
                                "content": null,
                                "headers": {
                                    "Location": "https://google.com"
                                },
                                "infinite_content": false
                            },
                            "/infinite" : {
                                "status_code": 200,
                                "content": "aaaa",
                                "headers": {
                                    "content-type": "text/html"
                                },
                                "infinite_content": true
                            }
                        }
                    }""")
        with open(test_config.name, "w") as f:
            f.write(config)
        self._start_server(test_config.name)
        self._wait_server_up(HTTP_SERVER_HOSTNAME, HTTP_SERVER_PORT)
        result = requests.get("http://%s:%s/test" % (HTTP_SERVER_HOSTNAME, HTTP_SERVER_PORT))
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.content, b"Test content")
        self._files_to_del.append(test_config.name)


    def test_multiple_redirects(self):
        self._start_server("./configs/config_multiredirect.json")
        self._wait_server_up(HTTP_SERVER_HOSTNAME, HTTP_SERVER_PORT)
        result = requests.head("http://%s:%s/redirect1" % (HTTP_SERVER_HOSTNAME, HTTP_SERVER_PORT), allow_redirects=True)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.url, "https://www.google.com/")
        result = requests.head("http://%s:%s/redirect1" % (HTTP_SERVER_HOSTNAME, HTTP_SERVER_PORT), allow_redirects=False)
        self.assertEqual(result.status_code, 301)


class TestURLResolver(BaseHttpTest):
    def test_multiple_redirects(self):
        self._start_server("./configs/config_multiredirect.json")
        self._wait_server_up(HTTP_SERVER_HOSTNAME, HTTP_SERVER_PORT)
        cmd_output = subprocess.check_output(["python3", "urlresolve.py", "http://%s:%s/redirect1" % (HTTP_SERVER_HOSTNAME, HTTP_SERVER_PORT)])
        self.assertTrue(b"https://www.google.com/" in cmd_output)

    def test_cycle_redirects(self):
        self._start_server("./configs/config_cycleredirect.json")
        self._wait_server_up(HTTP_SERVER_HOSTNAME, HTTP_SERVER_PORT)
        with self.assertRaisesRegex(subprocess.CalledProcessError, "returned non-zero exit status 1"):
            cmd_output = subprocess.check_output(["python3", "urlresolve.py", "http://%s:%s/redirect1" % (HTTP_SERVER_HOSTNAME, HTTP_SERVER_PORT)])

    def test_cycle_redirects_cycle_detect(self):
        self._start_server("./configs/config_cycleredirect.json")
        self._wait_server_up(HTTP_SERVER_HOSTNAME, HTTP_SERVER_PORT)
        cmd_output = subprocess.check_output(["python3", "urlresolve.py", "http://%s:%s/redirect1" % (HTTP_SERVER_HOSTNAME, HTTP_SERVER_PORT), "--method", "smart"])
        self.assertTrue(b"Cycled redirects are detected." in cmd_output)
        self.assertTrue(b"The final URL is: None" in cmd_output)

    def test_redirect_to_heavy_content(self):
        with open("./configs/config_base.json", "rb") as f:
            config = json.load(f)
        config[ENDPOINTS_KEY]["/redirect"]["headers"]["Location"] = "/infinite"
        test_config_file = tempfile.NamedTemporaryFile(delete=False)
        test_config_file.close()
        with open(test_config_file.name, "w") as f:
            json.dump(config, f)
        self._start_server(test_config_file.name)
        self._wait_server_up(HTTP_SERVER_HOSTNAME, HTTP_SERVER_PORT)
        cmd_output = subprocess.check_output(["python3", "urlresolve.py", "http://%s:%s/redirect" % (HTTP_SERVER_HOSTNAME, HTTP_SERVER_PORT)])
        self.assertTrue(b"/infinite" in cmd_output)
        self._files_to_del.append(test_config_file.name)


if __name__ == '__main__':
    unittest.main()