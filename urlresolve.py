import argparse
import logging
import requests
import sys

from urllib.parse import urlparse, ParseResult, urlunparse

from constants import messages_client

__all__ = ["resolve_url"]

log = logging.getLogger("Resolver")
logging.basicConfig(stream=sys.stdout, level=logging.INFO)

def resolve_url(url, method="naive"):
    if method == "naive":
        final_url = __resolve_url_naive(url)
    else:
        final_url = __resolve_url_smart(url)
    return final_url


def __resolve_url_naive(url):
    result = requests.head(url, allow_redirects=True)
    return result.url


def __resolve_url_smart(url):
    set_visited = set()
    parsed = urlparse(url)
    while True:
        if url in set_visited:
            log.info(messages_client.cycled_redirects % res.url)
            final_url = None
            break
        res = requests.head(url, allow_redirects=False)
        set_visited.add(url)

        if res.status_code not in {301, 302, 307, 308}:
            final_url = res.url
            break
        else:
            url = res.headers.get("location")
            parse_loc = urlparse(url)
            # relative URL in location header - trying to make it absolute
            if not parse_loc.netloc:
                parsed_with_changed_path = ParseResult(scheme=parsed.scheme,
                                                       netloc=parsed.netloc,
                                                       path=parse_loc.path,
                                                       params=parse_loc.params,
                                                       query=parse_loc.query,
                                                       fragment=parse_loc.fragment)
                url = urlunparse(parsed_with_changed_path)
    return final_url


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("url", help="URL to resolve redirections.")
    parser.add_argument("--method",
                         action="store",
                         help="""Method to resolve URL. Option has a 2 possible values - 'naive' (default) and 'smart'.
                                 * 'naive' - uses the requests allow_redirects and history to resolve URL
                                 * 'smart' - self-coded algorithm which can detect the cycled redirects""",
                         default="naive")
    args = parser.parse_args()
    try:
        final_url = resolve_url(args.url, method=args.method)
        log.info("The final URL is: %s" % final_url)
    except Exception as e:
        raise Exception("Failed to resolve URL. %s" % str(e))


